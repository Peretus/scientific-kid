import * as React from 'react';
import styled from 'styled-components';

const Suggestions = styled.ul`
 text-align: left;
 width: 200px;
 display: inline-block;
 list-style-type: default;
 margin-left: 50px;
 list-style-type: circle;
`;

const LandingPage = () => {
  return (
    <div id="landing-page">
      <h1>This is the landing page</h1>
      <div>
        <h2>We can put stuff here:</h2>
        <Suggestions>
          <li>List of new experiments</li>
          <li>Facebook updates</li>
          <li>Blog posts</li>
          <li>labcoat stuff</li>
        </Suggestions>
      </div>
    </div>
  );
};

export default LandingPage;
