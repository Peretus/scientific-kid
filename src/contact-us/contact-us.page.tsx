import * as React from 'react';
import { Row, Col} from 'antd';

const ContactUsPage = () => {
  return (
      <div className="contact-us">
      <Row gutter={18} className="contact-us-page-wrapper" type="flex" justify="center" align="top">
        <Col span={24}>
          <h1>This is the Contact Us page</h1>
        </Col>
      </Row>
    </div>
  );
};

export default ContactUsPage;
