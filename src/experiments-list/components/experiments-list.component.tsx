import { Row, Col } from 'antd';
import * as React from 'react';
import styled from 'styled-components';
import { Experiment } from '../../common/models/experiment.model';
import ExperimentCardWithRouter from '../../experiment-card/components/experiment-card.component';
import Masonry from 'react-masonry-component';

const MasonryLayout = styled(Masonry)`
  margin: 0 auto;
`;

type ExperimentsListProps = {
  experiments: Experiment[];
  onGetExperiments: () => null;
};

const masonryOptions = {
  fitWidth: true,
  transitionDuration: '0.3s'
};

class ExperimentsList extends React.Component<ExperimentsListProps, void> {
  componentDidMount() {
    if (!this.props.experiments.length) {
      this.props.onGetExperiments();
    }
  }
  render() {
    const { experiments } = this.props;
    return (
      <Row type="flex" justify="center">
        <Col span={24} >
          <MasonryLayout disableImagesLoaded={false} updateOnEachImageLoad={false} options={masonryOptions}>
            {experiments.map((experiment) => {
              return (<ExperimentCardWithRouter experiment={experiment}/>);
            })}
          </MasonryLayout>
        </Col>
      </Row>
    );
  }
}

export default ExperimentsList;


