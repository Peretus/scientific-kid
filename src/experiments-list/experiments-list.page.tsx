import * as React from 'react';
import { ExperimentsListContainer } from './../experiments-list/containers/experiments-list.container';


const ExperimentsList = () => {
  return (
      <ExperimentsListContainer />
  );
};

export default ExperimentsList;
