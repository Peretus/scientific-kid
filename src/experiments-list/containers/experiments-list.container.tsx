import { getAllExperiments } from '../../experiments/actions/experiments.actions';
import ExperimentsList from '../components/experiments-list.component';
import { connect } from 'react-redux';
import { AppState } from '../../persistence/app.state';

export const ExperimentsListContainer = connect(
  (state: AppState) => {
    return  ({
      experiments: state.experiments.experiments,
    });
  },
  (dispatch) => ({
    onGetExperiments: () => dispatch(getAllExperiments())
  })
)(ExperimentsList);