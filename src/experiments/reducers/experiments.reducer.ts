import { Experiment } from '../../common/models/experiment.model';
import { ExperimentsAction, ADD_ALL_EXPERIMENTS } from '../actions/experiments.actions';
export type ExperimentsState = {
  experiments: Experiment[];
};

const defaultExperiments = {
 experiments: [],
 pending: true,
};

export function experiments
  (state: ExperimentsState = defaultExperiments, action: ExperimentsAction)
  : ExperimentsState {
    switch (action.type) {
      case ADD_ALL_EXPERIMENTS:
        return {
          ...state,
          experiments: action.payload.experiments
        };
      default: {
        return state;
      }
    }
  }