import { Experiment } from '../../common/models/experiment.model';
import { ThunkAction } from 'redux-thunk';
import { db } from '../../persistence/database';
import { Action } from '../../common/types/action.type';

export const GET_ALL_EXPERIMENTS = 'GET_EXPERIMENTS';
export const ADD_ALL_EXPERIMENTS = 'ADD_ALL_EXPERIMENTS';

type AddAllExperiments = Action<typeof ADD_ALL_EXPERIMENTS, ExperimentsPayload >;

export type ExperimentsAction = AddAllExperiments;
export type ExperimentsPayload = {
  experiments: Experiment[];
};


export const addAllExperiments = (experiments: Experiment[]): ExperimentsAction => {
  return {
    type: ADD_ALL_EXPERIMENTS,
    payload: { experiments }
  };
};

export const getAllExperiments = ():  ThunkAction<void, {}, null> => {
  return (dispatch) => {
    const firebaseRef = db.ref();
    firebaseRef.once('value').then(
      (snapshot) => {
        dispatch(
          {
            type: ADD_ALL_EXPERIMENTS,
            payload: {experiments: snapshot.val()}
          }
        );
      });
    };
};