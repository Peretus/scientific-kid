import { Action as ReduxAction } from 'redux';

export interface Action<Type, Payload> extends ReduxAction {
  type: Type;
  payload: Payload;
  error?: boolean;
  meta?: object;
}