export type ExperimentStep = {
  instructions: string;
};
export type ExperimentSupply = {
  name: string;
  description: string;
};
export type KeyTerm = {
  term: string;
  definition: string;
};
export type Experiment = {
  imageURL: string;
  title: string;
  slug: string;
  id: number;
  steps: ExperimentStep[];
  supplies: ExperimentSupply[];
  summary: string;
  terms?: KeyTerm[];
};
