import firebase from 'firebase';

try {
  const config = {  
    apiKey: 'AIzaSyAHzjKyBxbL44ZDQf5pR_vr7npokh8U9fU',
    authDomain: 'scientific-kid.firebaseapp.com',
    databaseURL: 'https://scientific-kid.firebaseio.com',
    projectId: 'scientific-kid',
    storageBucket: 'scientific-kid.appspot.com',
    messagingSenderId: '675829899398'
  };

  firebase.initializeApp(config);
} catch (e) {
  console.log("Error!: ", e);
}

export const db = firebase.database();
export default firebase;