import {combineReducers} from 'redux';
import { experiments } from '../experiments/reducers/experiments.reducer';

const rootReducer = combineReducers({experiments});

export default rootReducer;