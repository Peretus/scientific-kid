import { ExperimentsState } from '../experiments/reducers/experiments.reducer';
export interface AppState {
  experiments: ExperimentsState;
}