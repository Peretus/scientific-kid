import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, autoRehydrate} from 'redux-persist';
import thunk from 'redux-thunk';
// import createLogger from 'redux-logger';
import rootReducer from './root.reducer';

// const logger = createLogger();

const store = createStore(
  rootReducer,
  {}, compose(
    applyMiddleware(thunk),
    autoRehydrate()
  )
);

persistStore(store);

export default store;
