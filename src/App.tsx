import * as React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import LandingPage from './landing/landing.page';
import Header from './header/components/header.component';
import ExperimentsList from './experiments-list/experiments-list.page';
import ContactUsPage from './contact-us/contact-us.page';
import ExperimentDetailPage from './experiment-detail/experiment-detail.page';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

class App extends React.Component<{}, null> {
  render() {
    return (
      <Router>
        <span className="App">
          <Header />
          <Switch>
            <Route path={'/'} exact={true} component={LandingPage}/>
            <Route path={'/contact'} component={ContactUsPage}/>
            <Route path={'/experiments/:slug'} component={ExperimentDetailPage}/>
            <Route path={'/experiments'} component={ExperimentsList}/>
          </Switch>
        </span>
      </Router>
    );
  }
}

export default App;
