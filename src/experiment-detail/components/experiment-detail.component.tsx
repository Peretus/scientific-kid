import { Experiment } from '../../common/models/experiment.model';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Collapse } from 'antd';

import styled from 'styled-components';
const Panel = Collapse.Panel;

type DetailProps = {
  experiment: Experiment;
};

const StepsList = styled.ol`
  display: block;
  list-style-type: decimal;
  margin-top: 2em;
  margin-bottom: 1em;
  margin-left: 0;
  margin-right: 0;
  padding-left: 40px;
  text-align: left;
  font-weight: bold;
  // background-color: lightBlue;
`;

const StepsItem = styled.li`
  margin-top: 2em;
  text-align: left;
  font-weight: bold;
  border: 1px solid black;
  // background-color: lightYellow;
`;

// const StepsTitle = styled.h3`
//   font-weight: bold;
//   background-color: lightGrey;
// `;


class ExperimentStepsList extends React.Component<DetailProps, void> {
  render() {
    return(
      <div>
        <StepsList>
          {this.props.experiment.steps.map((step) => {
            return (<StepsItem>{step.instructions}</StepsItem>);
          })}
        </StepsList>
      </div>
    );
  }
}

const ContentRow = styled.div`
  min-height: 1200px;
`;
const TitleSectionRow = styled.div`
`;
const ImageSectionRow = styled.div`
`;

const Content = styled.div`
  // background-color: lightPink;
  width: 95%;
  margin: 0 auto;
`;
const ImageSectionContent = styled.div`
  // background-color: salmon;
`;
const TitleSectionContent = styled.div`
  text-align: left;
  // background-color: lightYellow;
`;

const ClockIcon = styled(Icon)`
  font-size: 2em;
  margin: 0;
`;

const TimeToCompleteSection = styled.div`
`;
const ExperimentSection = styled.div`
  width: 70%;
  display: inline-block;
`;
const KeywordSection = styled.div`
  width: 30%;
  display: inline-block;
`;

const TimeWrapperWithBorder = styled.span`
  border-bottom: 2px solid black;
  position: relative;
  bottom: 3px;
  margin-left: 3px;
`;
const Time = styled.span`
  position: relative;
  bottom: 2px;
`;

const ExperimentIdSection = styled.span`
  position: relative;
  bottom: 5px;
  font-size: 1.5em;
`;

const ExperimentTimeSection = styled.span`
  white-space: nowrap;
`;
const ExperimentId = styled.span`
  margin-right: 15px;
  position: relative;
  bottom: -1px;
`;
const ExperimentLabel = styled.span`
  margin-right: 10px;
`;
const TitleInfoSection = styled.span`
  display: inline-block;
  width: 70%;
`;
const ShareIconsSection = styled.span`
  width: 30%;
  display: inline-block;
  text-align: right;
`;
const HeartIcon = styled(Icon)`
  font-size: 50px;
  margin: 0;
  color: #f6a1a8;
  font-size: 2em;
`;

const ShareIcon = styled(Icon)`
  font-size: 50px;
  margin: 0;
  color: #f6a1a8;
  font-size: 2em;
  margin-right: 10px;
`;
const Keywords = styled.div`
  width: 26%;
  position:absolute;
  left: 70.33%;
  top:186px;
  // right: 0;
  
  .ant-collapse {
        :firstChild {
          border: '1px solid black';
        }
    .ant-collapse-item {
      border: none;
      .ant-collapse-header {
        margin-top: 10px;
        color: blue
      }
    }
  }
`;

const HeroImage = styled.img`
  width: 100%;
  max-height: 300px;
  overflow: hidden;
  border: 2px solid black;
  background-color: salmon;
`;

const keywordsStyle = {
  background: 'white',
  marginBottom: 24,
  border: '2px solid black',
  borderRadius: 0,
  // color: 'inherit',
  textAlign: 'left'
};

const keywordsPanelStyle = {
  background: 'white',
  marginBottom: 10,
  textAlign: 'left'
};




class ExperimentDetail extends React.Component<DetailProps, void> {
  render() {
    const { experiment } = this.props;
    let container;
    return (
      <ContentRow ref={(node) => { container = node; }}>
        <Content>
          <ExperimentSection>
            <TitleSectionRow>
              <TitleSectionContent>
                <div><Link to={'/experiments'}>Go Back</Link></div>
                <TitleInfoSection>
                  <h1>Do A Very Cool Thing</h1>
                  {experiment.steps && (
                    <TimeToCompleteSection>
                      <ExperimentIdSection>
                        <ExperimentLabel>experiment</ExperimentLabel>
                        <ExperimentId>626</ExperimentId>
                      </ExperimentIdSection>
                      <ExperimentTimeSection>
                        <ClockIcon type="clock-circle-o" />
                        <TimeWrapperWithBorder><Time>{experiment.steps.length + 12} mins</Time></TimeWrapperWithBorder>
                      </ExperimentTimeSection>
                    </TimeToCompleteSection>
                  )}
                </TitleInfoSection>
                <ShareIconsSection>
                  <ShareIcon type="export" />
                  <HeartIcon type="heart-o"/>
                </ShareIconsSection>
              </TitleSectionContent>
            </TitleSectionRow>
            <ImageSectionRow>
              {experiment.steps && (
                <ImageSectionContent>
                  <HeroImage src={experiment.imageURL} />
                  <p>{experiment.summary}</p>
                  <ExperimentStepsList experiment={experiment} />
                </ImageSectionContent>)}
              )
            </ImageSectionRow>
          </ExperimentSection>
          <KeywordSection>
            <Keywords>
              <Collapse accordion={true} style={keywordsStyle}>
                <Panel header={'This is panel header 1'} style={keywordsPanelStyle} key="1">
                  <p>{experiment.summary}</p>
                </Panel>
                <Panel header={'This is panel header 2'} style={keywordsPanelStyle} key="2">
                  <p>{experiment.summary}</p>
                </Panel>
                <Panel header={'This is panel header 3'} style={keywordsPanelStyle} key="3">
                  <p>{experiment.summary}</p>
                </Panel>
              </Collapse>
            </Keywords>
          </KeywordSection>
        </Content>
      </ContentRow>
    );
  }
}

export default ExperimentDetail;