import * as React from 'react';
import ExperimentDetailContainer from './../experiment-detail/containers/experiment-detail.container';
import {match, RouteComponentProps} from 'react-router-dom';
interface ExperimentDetailPageProps extends RouteComponentProps<any> {
  match: match<any>
}

class ExperimentDetailPage extends React.Component<ExperimentDetailPageProps, void> {
  render() {
    // const { location } = this.props;
    return (
      <ExperimentDetailContainer slug={this.props.match.params.slug}/>
    );
  }
}


export default ExperimentDetailPage;
