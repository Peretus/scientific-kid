import { getAllExperiments } from '../../experiments/actions/experiments.actions';
import ExperimentDetail from '../components/experiment-detail.component';
import { connect } from 'react-redux';
import { AppState } from '../../persistence/app.state';
import { find } from 'lodash';

const ExperimentDetailContainer = connect(
  (state: AppState, ownProps) => {
    return  ({
      experiment: state.experiments.experiments.length ?
        find(state.experiments.experiments, ['slug', ownProps.slug]) || {} :
        {}
    });
  },
  (dispatch) => ({
    onGetExperiments: () => dispatch(getAllExperiments())
  })
)(ExperimentDetail);

export default ExperimentDetailContainer;