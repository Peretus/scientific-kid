import { Icon } from 'antd';
import * as React from 'react';
import styled from 'styled-components';
import { Experiment } from '../../common/models/experiment.model';
import { withRouter, RouteComponentProps } from 'react-router-dom';

const ExperimentCardWrapper = styled.div`
  background-color: white;
  width: 300px;
  margin: 0 15px;
`;

const ExperimentCardNumber = styled.span`
  background: white;
  color: black;
  float: left;
  margin-left: 10px;
  top: -9px;
  position: relative;
  padding: 3px;
  padding-top: 0;
`;

const ExperimentCardBorder = styled.div`
  border: 3px solid black;
`;

const ExperimentCardContent = styled.div`
  text-align: center;
`;

// const ExperimentCardImage = styled.img`
//   width:100%;
//   margin-top: -9px;
//   height: auto;
//   border-top: 2px solid black;
//   border-bottom: 2px solid black;
// `;

const ExperimentCardTitle = styled.h1`
  font-size: 12px;
  font-weight: bold;
  text-align: left;
  text-transform: uppercase;
  padding: 10px;
  padding-bottom: 0;
`;

const ExperimentCardSummary = styled.div`
  text-align: left;
  padding: 10px;
  padding-top: 0;
`;

const ExperimentCardCompletionSummary = styled.div`
  text-align: left;
  padding: 10px;
  padding-bottom: 7px;
`;

const ClockIcon = styled(Icon)`
  font-size: 50px;
  margin: 0;
`;

const ExperimentCardCompletionTime = styled.span`
  min-width: 25%;
  top: -20px;
  position: relative;
  display: inline-block;
  border-bottom: 2px solid gray;
`;

const ExperimentCardFavoriteButton = styled.span`
  font-size: 50px;
  float: right;
  position: relative;
  top: -13px;
`;

const HeartIcon = styled(Icon)`
  font-size: 50px;
  margin: 0;
  color: #f6a1a8;
`;

export interface ExperimentCardProps extends RouteComponentProps<any> {
  experiment: Experiment;
}

/*const ExperimentCard = (experiment: Experiment) => {
  return (
    <ExperimentCardWrapper>
      <ExperimentCardNumber>experiment {experiment.id}</ExperimentCardNumber>
      <ExperimentCardBorder>
        <ExperimentCardContent>
          <ExperimentCardImage src={experiment.imageURL}/>
          <ExperimentCardTitle>{experiment.title}</ExperimentCardTitle>
          <ExperimentCardSummary>{experiment.summary}</ExperimentCardSummary>
          <ExperimentCardCompletionSummary>
            <ClockIcon type="clock-circle-o" />
            <ExperimentCardCompletionTime>12 min</ExperimentCardCompletionTime>
            <ExperimentCardFavoriteButton>
              <HeartIcon type="heart-o"/>
            </ExperimentCardFavoriteButton>
          </ExperimentCardCompletionSummary>
        </ExperimentCardContent>
      </ExperimentCardBorder>
    </ExperimentCardWrapper>
  );
};*/
class ExperimentCard extends React.Component<any, void> {
  render() {
    const { experiment } = this.props;
    return (
        <ExperimentCardWrapper onClick={(e) => {this.props.history.push('experiments/' + experiment.slug); }}>
          <ExperimentCardNumber>experiment {experiment.id}</ExperimentCardNumber>
          <ExperimentCardBorder>
            <ExperimentCardContent>
              {/*<ExperimentCardImage src={experiment.imageURL}/>*/}
              <ExperimentCardTitle>{experiment.title}</ExperimentCardTitle>
              <ExperimentCardSummary>{experiment.summary}</ExperimentCardSummary>
              <ExperimentCardCompletionSummary>
                <ClockIcon type="clock-circle-o" />
                <ExperimentCardCompletionTime>12 min</ExperimentCardCompletionTime>
                <ExperimentCardFavoriteButton>
                  <HeartIcon type="heart-o"/>
                </ExperimentCardFavoriteButton>
              </ExperimentCardCompletionSummary>
            </ExperimentCardContent>
          </ExperimentCardBorder>
        </ExperimentCardWrapper>
    );
  }
}

const ExperimentCardWithRouter = withRouter(ExperimentCard);
export default ExperimentCardWithRouter;


