import * as React from 'react';
import { Row, Col} from 'antd';
import styled from 'styled-components';
const logo = require('../../logo.svg');
type HeaderComponentProps = {

};

const Logo = styled.img`
  height: 100px;
`;

const HeaderSection = styled(Row)`

`;
const LogoSection = styled(Col)`

`;
const HeaderMenuSection = styled(Col)`

`;

const DropdownHeaderMenuSection = styled(Col)`

`;


class Header extends React.Component<HeaderComponentProps, {}> {
  render() {
    return (
      <HeaderSection>
      <LogoSection span={2}><Logo src={logo} alt="Scientific Kid Logo" /></LogoSection>
      <HeaderMenuSection span={20}>This is where the menu goes</HeaderMenuSection>
      <DropdownHeaderMenuSection span={2}>Dropdown here</DropdownHeaderMenuSection>
      </HeaderSection>
    );
  }
}

export default Header;